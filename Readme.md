## Scripts

* `init_env.sh` (export variables from .env file in system);
* `start_test.sh` (start unit test for mapper and reducer)

## Problem
Twitter msgs with delete events put into elasticsearch, aggreagate and vizualize with grafana for count of
messages per minutes

## Requirements

* Java 8
* Python > 3.4
* Elasticsearch 6.5
* Logstash 6.5
* Graphana 5.3.4
* Optional Docker-compose
* Vagrant

# Start

###Start local

* `run elasticsearch`
* `logstash ./logstash.conf`
* `run graphana`
* `pip3 install -r requirements.txt`
* `python3 tweeter_consumer.py`

###Start docker-compose

* `docker-compose up -d`
* `pip3 install -r requirements.txt`
* `python3 tweeter_consumer.py`

###Start vagrant

* `vagrant plugin install vagrant-docker-compose`
* `vagrant up`
* see result on 127.0.0.1:8047