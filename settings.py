from envparse import env
from logging.config import dictConfig


env.read_envfile()

# Tweeter access tokens
ACCESS_TOKEN = env('ACCESS_TOKEN', default='')
ACCESS_TOKEN_SECRET = env('ACCESS_TOKEN_SECRET', default='')
CONSUMER_KEY = env('CONSUMER_KEY', default='')
CONSUMER_SECRET = env('CONSUMER_SECRET', default='')


# Config of logger with logstash
LOGGING = {
  'version': 1,
  'disable_existing_loggers': True,
  'formatters': {
      'default': {
            'format': '[%(asctime)s][%(levelname)s] %(name)s '
                       '%(filename)s:%(funcName)s:%(lineno)d || %(message)s',
        },
        'simple': {
            'format': 'velname)s %(message)s'
        },
  },
  'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'logstash': {
            'level': 'INFO',
            'class': 'logstash.TCPLogstashHandler',
            'host': 'localhost',
            'port': 5044,
            'version': 1,
            'message_type': 'tweet',
            'fqdn': False,
            'tags': ['tweet'],
        },
  },
  'loggers': {
        '__main__': {
            'handlers': ['logstash', 'console'],
            'level': 'DEBUG',
        },
        'tweeter_consumer': {
            'handlers': ['logstash', 'console'],
            'level': 'DEBUG',
        }

    }
}

dictConfig(LOGGING)
