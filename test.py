import pytest # noqa
import settings # noqa
from tweeter_consumer import TweetListener
import logging
import datetime


def test_logs_on_error():
    """
    Test check error while handling tweeter stream
    :return:
    """
    twl = TweetListener()

    assert twl.on_error(400)

    assert not twl.on_error(420)


def test_logs_on_data(caplog):
    """
    Test that msg from tweeter in logging
    :param caplog Fixture logger
    :return:
    """
    twl = TweetListener()
    caplog.set_level(logging.DEBUG)

    assert twl.on_data('{"delete": {"timestamp_ms": 1543610251509}}')
    assert len(caplog.records) == 0

    assert twl.on_data('{"delete": {"timestamp_ms": 1543610280}}')
    assert len(caplog.records) == 1

    caplog.clear()

    count = 1

    for i in range(10):
        timstamp = datetime.datetime.timestamp(datetime.datetime.now())
        assert twl.on_data('{"delete": {"timestamp_ms": %s}}' % timstamp)
        assert twl.count == count + 1
        count += 1
        assert len(caplog.records) == 1

