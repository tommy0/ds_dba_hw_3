from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import logging
import json
import settings
from datetime import datetime

app_log = logging.getLogger(__name__)


class TweetListener(StreamListener):
    save_timestamp: int = None
    count: int = 0

    def on_data(self, raw_data: str) -> bool:
        """
        Function which call after all data events, we needs only deleted event
        :param raw_data: json data from tweeter
        :return: Succesfull of processing operation
        """
        data = json.loads(raw_data)

        if 'delete' in data:
            # [A. SELIVANOVSKIKH 12/1/2018] Because tweeter send timestamp in ms
            timestamp_ms = int(data['delete']['timestamp_ms']) / 1000
            custom_timestamp = int(datetime.timestamp(
                datetime.fromtimestamp(timestamp_ms).replace(microsecond=0, second=0)))

            if not self.save_timestamp:
                self.save_timestamp = custom_timestamp

            if self.save_timestamp != custom_timestamp:
                app_log.info(f'{self.save_timestamp} - {self.count}',
                             extra={'custom_timestamp': custom_timestamp, "count": self.count})
                self.count = 1
                self.save_timestamp = custom_timestamp

            self.count += 1

        return True

    def on_error(self, status: int) -> bool:
        """
        Function which call when tweeter api return response with error code
        :param status: code error of response
        :return: Succesfull of processing operation
        """
        print(status)
        # [A. SELIVANOVSKIKH 12/1/2018] Because 420 is session aborted, then program crash
        if status == 420:
            return False
        return True


def init_app():
    """
    Init tweeter stream api, auth with tokens, and register consumer
    :return:
    """
    l = TweetListener()
    auth = OAuthHandler(settings.CONSUMER_KEY, settings.CONSUMER_SECRET)
    auth.set_access_token(settings.ACCESS_TOKEN, settings.ACCESS_TOKEN_SECRET)
    stream = Stream(auth, l)
    stream.sample()


if __name__ == '__main__':
    init_app()
